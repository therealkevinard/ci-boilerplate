> **Here be dragons.**  
  This is a boilerplate, mostly meant for personal use.  
  Much of it is patched-in from project(s) I'm working on rn, or half-done things that I (myself) know what to move where. 
  Copy-Paste or as-is use is an **awful** idea. Definitely check variables and scripts and things like that for project-specific setup. 

# Goal 
I like Concourse-CI, but - like basically any dev tooling - it takes some setup. I'm working towards an opinionated default to be _something close to_ "set it and forget it". This may be useful to you, but (the dragons) it's _goal_ is to be opinionated, and those opinions are mine - I like Makefiles, Layered docker-compose stacks, env vars, monorepos, and things like that. 

# General Layout 
- `_repo` is a template for a working repo. This would be where you `git init` a new project.  
It includes makefiles and the dirs that are important to the pipeline, but nothing regarding actual work files.  
The embedded docker-compose stack(s), on the other hand - be mindful of their volume mounts. 
  - `ci` holds project-specific ci config  
    It should be tailored and placed in your project's `ci` directory or a project-specific sidecar repo. 
  - `e2e` is project-specific e2e testing.  
    I like cypress. It has cypress. 
- `support` belongs to _this_ repo - it's overarching helpers. rn - solely the do-all Buildbox image  
  - `dockerfiles/buildbox` is the do-all buildbox image 
  - `concourse-server` is _not_ a concourse server. It's a docker-compose overlay that applies on top of https://github.com/concourse/concourse-docker to allow embedded s3 via minio. CC-CI/S3/Minio is thoroughly documented. Just use `endpoint: http://minio:9000` when you're running the service overlay.


# [Buildbox](./support/dockerfiles/buildbox)

## My do-everything ci builder. 

do-everything = less image pulls <3

## Usage Hints 

`docker pull registry.gitlab.com/therealkevinard/ci-boilerplate/buildbox:latest` 

### Mount local kubeconfig
mounting your personal kubeconfig to /root/.kube/config allows kubectl with local creds from inside container  

docker usage:  
`docker run --rm -it -v ~/.kube/config:/root/.kube/config registry.gitlab.com/therealkevinard/ci-boilerplate/buildbox bash`

fly-cli usage:  
`fly sp -p pipeline -c pipeline.yml -l secrets.yml -v "kube_config=$(cat ~/.kube/config)"` 

_hint: to preserve line breaks, quote the var content in an echo_
```
mkdir -p /root/.kube
echo "$kubeconfig" > /root/.kube/config # quotes are important
# setting -ex before echoing kubeconfig puts content in logs. no-no. 
set -ex 
kubectl get pod --all-namespaces
```

### Someone told you to put basically everything in a volume mount? 

There are two helper scripts in /usr/local/bin for when you're stuck in bad deplopments. (eg: you're overusing volume mounts and need simple rsync for deploy)
- `find-pod $namespace $label-selector` gets a deployment, and returns the name of its first pod 
- `krsync` is a wrapper that allows reverse-tunnel rsync from a destination pod. (ofc, rsync must be available on the pod)

The two go together a la: 

```
ns="some-ns"
selector="app=lamp-stack,service=apache"
pod=$(find-pod $ns $selector) 
echo $pod # name of the first pod in the matched deployment 

krsync -av --progress --stats \
    path/to/files/ \
    ${pod}@${ns}:/app/path/to/files/
```