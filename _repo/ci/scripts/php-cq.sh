#!/bin/bash
# set -ex #set -e is the direct way to exit on inner failures. For this case, I'd rather proceed with the other checks and return errors afterward.
set -x

# vars - these are largely not necc., but... ci is the place for verbosity, yeah? 
basepath=$(pwd)
monorepo=${basepath}/ddc-monorepo
reports=${basepath}/reports-php
extensions=${monorepo}/_mono/files/plugins/trka

# cq
php-cs-fixer fix ${extensions} --rules=@PhpCsFixer,@PSR2

phpmd ${extensions} xml \
    cleancode,controversial,design,codesize,unusedcode,naming \
    > ${reports}/phpmd.xml

mkdir -p ${reports}/pdepend
pdepend --summary-xml=${reports}/pdepend/summary.xml \
    --jdepend-chart=${reports}/pdepend/jdepend.svg \
    --overview-pyramid=${reports}/pdepend/pyramid.svg \
    ${extensions}

mkdir -p ${reports}/psecio 
psecio-parse scan ${extensions} --no-ansi > ${reports}/psecio/log.txt


#-- bring it all together 
mkdir -p ${reports}/analyze
analyze analyze ${extensions} \
    --phpmd=${reports}/phpmd.xml \
    --pdepend=${reports}/pdepend/summary.xml \
    --exclude_analyzers=git,gitDetailed
analyze bundle ${reports}/analyze

cd ${basepath}