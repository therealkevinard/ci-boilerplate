#!/bin/bash
set -ex

# vars 
basepath=$(pwd)
theme_dir=${basepath}/ddc-monorepo/_mono/files/themes/kowabunga 
clean_theme=${basepath}/clean-theme

# cq 
cd ${theme_dir}
npm install
# npm audit fix --only=prod > ${reports}/npm-audit-log.txt # TODO
npm run pretty 

cp -R ${theme_dir} ${clean_theme}

cd ${basepath}