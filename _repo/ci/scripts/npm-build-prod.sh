#!/bin/bash
set -ex

basepath=$(pwd)
clean_theme=${basepath}/clean-theme
reports=${basepath}/reports-npm
theme_dist=${basepath}/theme-dist

cd ${clean_theme}/kowabunga
npm install jquery@1.9.1 popper.js@1.16.1 mocha@3.1.2 mocha@7
npm install # pref install over ci to preserve previous node_modules 
npm run build:prod

mv ${clean_theme}/kowabunga/resources/report.html ${reports}/webpack-bundle.html
cp -R ${clean_theme}/kowabunga ${theme_dist}

cd ${basepath}