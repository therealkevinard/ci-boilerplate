#!/bin/bash
set -eux

# entrypoint for e2e stack cypress service. prepares cypress install and runs test suites 
# this obv doesn't run locally, but the environments are synced. 
# to run local equivalent, use `cd $project/e2e && npm run cy:open`

cd /home/node

# clean artifacts 
rm -rf cypress/screenshots/* cypress/videos/*

#-- deps 
npm install
# local cypress is a wrapper around /root/.cache/Cypress/$(local-version)/Cypress. definitely cache/volume this dir 
$(npm bin)/cypress install
$(npm bin)/cypress verify
$(npm bin)/cypress info

npm run test
