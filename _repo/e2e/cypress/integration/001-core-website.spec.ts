describe('Core website with standard marketing/sales pages', () => {

    const adminuser = { username: 'admin', password: 'admin' }

    /* describe.skip('Admin Login', () => {
        beforeEach(() => {
            cy.visit('/rwm_admin')
        })

        it('allows authorized login and logout', () => {
            cy.loginUi('admin', 'admin')
            cy.url().should('be', 'http://localsite.com/rwm_admin/backend')
            cy.logout()
            cy.url().should('be', 'http://localsite.com/rwm_admin/backend/auth/signin')
        })

        it('prevents invalid login', () => {
            cy.get('input[name="login"]').type('badusername')
            cy.get('input[name="password"]').type(`${adminuser.password}{enter}`)
            cy.get('.flash-message.error')
            cy.url().should('not.be', 'http://localsite.com/rwm_admin/backend')
        })
    }) */

    describe('Basic Frontend Frame', () => {
        before(() => {
            cy.visit('/')
        })

        describe('Page Meta', () => {
            it('has site title in window title', () => {
                cy.get('head title')
                    .should('contain.text', 'Kowabunga Comics')
            })
        })

        describe('Page Frame', () => {
            it('nav drawer toggle works', () => {
                cy.get('#trigger-nav').click()
                cy.get('.nav-drawer-inner').should('be.visible')

                cy.get('#trigger-nav').click()
                cy.get('.nav-drawer-inner').should('not.be.visible')
            })

            it('has complete nav container', () => {
                cy.get('[data-t="nav-outer"]').as('nav-outer')
                cy.get('@nav-outer').within(($nav) => {
                    cy.get('[data-t="link-home"] svg')
                    cy.get('[data-t="form-search-header"]')
                    cy.getByData('t', 'cart-menu')
                    cy.get('#notify-pull-list')
                    cy.get('#notify-cart')
                })
            })

            it('has id\'d header and footer', () => {
                cy.get('header').should('have.id', 'layout-header')
                cy.get('footer').should('have.id', 'layout-footer')
            })
        })
    })

})
