import { testUsers } from '../support/harness'
const testUserLogin = testUsers.customer

const profile_original = {
    name: 'Test User 001',
    phone_no: '423-381-4798',
    address_1: '3608 indian trail',
    address_2: '...',
    city: 'Chattanooga',
    postcode: '37412',
}

const profile_updated = {
    name: 'Foo Bar',
    phone_no: '212-555-1212',
    address_1: '303 Some Ave',
    address_2: 'Apt 7',
    city: 'Somecity',
    postcode: '11111',
}

const getNewUser = () => {
    const seed = Math.floor(Math.random() * 10000)
    return {
        name: `Test User ${seed}`,
        email: `test+${seed}@domain.tld`,
        password: `test+${seed}@domain.tld`,
    }
}

describe('users: basic frontend profile', () => {
    beforeEach(() => {
        cy.visit('/')
    })

    // skip for now because of user-register throttling
    /* it('user can self-register (basic)', () => {
        const profile_create = getNewUser()

        cy.getByData('t', 'link-signup').should('contain.text', 'Sign Up')
        cy.getByData('t', 'link-signup').click()
        cy.url().should('eq', 'http://localsite.com/account')
        cy.getByData('t', 'form-register').within(form => {
            cy.transposeFormData(profile_create)
            cy.get('button[type="submit"]').click()
        })
        cy.getByData('t', 'welcome-user').should('contain.text', profile_create.name, { matchCase: false })
        cy.getByData('t', 'link-logout').click() // logout
        cy.getByData('t', 'link-account').should('contain.text', 'Login/Register')
        cy.frontendLoginUi(profile_create.email, profile_create.password)
        cy.getByData('t', 'link-logout').click()
    }) */

    it('user has frontend login and logout', () => {
        cy.frontendLoginUi(testUserLogin.username, testUserLogin.password)

        // redirect after login, with Hi message in header
        cy.wait(500) // this is fragile, but we need to force-wait for login redirect and page ready
        cy.get('#trigger-nav').click()
        cy.get('.nav-drawer-inner').should('be.visible')
        cy.getByData('t', 'welcome-user').should('contain.text', 'Hi, ')

        // account link goes to account update page, and the fields are populated accurately
        cy.getByData('t', 'link-account').click()
        cy.get('input[name="email"]').should('have.value', testUserLogin.username)

        // redirect after logout, with proper login/register message in header
        cy.wait(500)
        cy.get('#trigger-nav').click()
        cy.getByData('t', 'link-logout').click()
        cy.getByData('t', 'link-login').should('exist')
    })

    it('user can update profile information, and persist across sessions', () => {
        // TODO: test email/password updates
        // TODO: test odd edge cases and error inputs
        // TODO: test case-insensitivity - link should be title case regardless of name input case

        const confirmProfile = (fieldValues: any) => {
            for (let field in fieldValues) {
                cy.get(`input[name="${field}"]`).should('have.value', fieldValues[field])
            }
        }

        const fullCycleProfileUpdateConfirm = (fieldValues: any) => {
            // Full-cycle complete account update, logout, login, confirm
            cy.wait(500)
            cy.get('#trigger-nav').click()

            cy.getByData('t', 'welcome-user').should('contain.text', 'Hi, ') // wait for redirect
            cy.getByData('t', 'link-account').click() // go to accct page
            cy.transposeFormData(fieldValues)
            cy.getByData('t', 'submit-account').click()
            cy.get('h3.rotating-banner-heading') // wait for redirect

            cy.getByData('t', 'link-logout').click({ force: true }) // logout
            cy.frontendLoginUi(testUserLogin.username, testUserLogin.password) // login
            cy.getByData('t', 'welcome-user').should('contain.text', fieldValues.name, { matchCase: false }) // acct link shows new name?
            cy.getByData('t', 'link-account').click({ force: true }) // return to profile page
            confirmProfile(fieldValues) // check all fields
        }

        cy.frontendLoginUi(testUserLogin.username, testUserLogin.password) // login
        fullCycleProfileUpdateConfirm(profile_updated)
        cy.getByData('t', 'link-logout').click({ force: true }) // logout

        cy.frontendLoginUi(testUserLogin.username, testUserLogin.password) // login
        fullCycleProfileUpdateConfirm(profile_original)
        cy.getByData('t', 'link-logout').click({ force: true }) // logout
    })


    it.skip('frontend rejects invalid login', () => {
        // TODO having trouble asserting on error flash-msg.
    })
})

