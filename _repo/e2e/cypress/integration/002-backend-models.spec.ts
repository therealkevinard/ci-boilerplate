import { generateRoutesTests } from '../support/harness'

describe.skip('basic admin routes', () => {
    before(() => {
        cy.loginIfNeeded()
    })
    after(() => {
        cy.logout()
    })

    describe('extension dependencies', () => {
        it('users extension is installed/available', () => {
            cy.visit('rwm_admin/rainlab/user/users')
            cy.title().should('include', 'Manage Users')
        })
    })

    // ns: trka/comicswarehouse
    describe('module: trka/comicswarehouse', () => {
        generateRoutesTests('rwm_admin/trka/comicswarehouse/items', 'Item')
        generateRoutesTests('rwm_admin/trka/comicswarehouse/contributors', 'Contributor')
        generateRoutesTests('rwm_admin/trka/comicswarehouse/distributors', 'Distributor')
        generateRoutesTests('rwm_admin/trka/comicswarehouse/publishers', 'Publisher')
        generateRoutesTests('rwm_admin/trka/comicswarehouse/shipments', 'Shipment')
    })
})

describe.skip('inventory.items: crud controls', () => {
    before(() => {
        cy.loginIfNeeded()
    })
    after(() => {
        cy.logout()
    })

    // TODO: finish this when there's more of a frontend. The oc backend is pretty tricky for e2e here.
    it.skip('can create items', () => {
        cy.visit('/rwm_admin/trka/comicswarehouse/items/create')
        cy.get('form.layout').within(form => {
            cy.transposeFormData({
                'Item[title]': 'robot item - halloween, 2000',
                'Item[retail_price]': '10.00',
                'Item[upc_code]': '123456789',
                'Item[distributor_item_number]': 'xxx-xxxx',
                'Item[inventory_current]': '20',
            })
            cy.contains('Create and close').click()
            cy.url().should('eq', 'http://localsite.com/rwm_admin/trka/comicswarehouse/items')
            cy.get('.list-cell-name-upc_code').contains('123456789', { timeout: 10000 })
        })
    })
})
