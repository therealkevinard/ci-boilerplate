describe('Admin control over customer orders', () => { })

describe('Allow Business Rules to grant conditional discounts', () => { })

describe('Allow Ephemeral profiles for anonymous orders', () => { })

describe('Catalog Items search/filter/index frontend', () => { })

describe('Checkout Screen', () => { })

describe('Customer Order Management', () => { })

describe('Customer Profiles', () => { })

describe('Integrate with Authorize.net', () => { })

describe('Intelligent support for Catalog Items and their availabilities', () => { })

describe('Register Mailgun sender for consistent delivery of critical messaging', () => { })

describe('ShipStation Integration', () => { })

describe('Stock management, back-house', () => { })

describe('Support bulk import', () => { })

describe('Support csv export for publisher orders', () => { })

describe('User Subscriptions, Stored Searches', () => { })

