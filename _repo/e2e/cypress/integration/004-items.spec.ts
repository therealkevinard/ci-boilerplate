describe('products frontend', () => {
    before(() => {
        cy.visit('/items-list')
    })

    it('product cards are complete', () => {
        cy.getByData('t', 'item-card').should('have.length.gt', 3)
        cy.get('[data-t="item-card"]')
            .first()
            .within(card => {
                cy.get('img').should('have.attr', 'src') // has image
                cy.get('.pinned.bottom.right')
                cy.contains('%').should('have.class', 'item-price-discount') // has percent off badge

                cy.get('.card-title a').should('have.attr', 'href').and('contain', '/items/') // has linked title

                cy.get('.left').within(() => {
                    cy.get('.item-price-main')
                    cy.get('.item-price-retail')
                })

                // cart-add is present with all inputs
                cy.getByData('t', 'cart-add--item-card').within(() => {
                    cy.get('[name="itemid"]')
                    cy.get('[name="count"]')
                })
            })
    })

    it('has product flow with master/detail views', () => {
        cy.get('.card-title > a').first().then(titleLink => {
            const title = titleLink.text().trim().substr(0, 10)
            cy.get('.card-title > a').first().click()
            cy.url().should('contain', '/items/')
            cy.get('h1.col-sm-12').should('exist')
            cy.contains(title).should('exist')
        })
    })
})
