// ***********************************************************
// This example support/index.js is processed and
// loaded automatically before your test files.
//
// This is a great place to put global configuration and
// behavior that modifies Cypress.
//
// You can change the location of this file or turn off
// automatically serving support files with the
// 'supportFile' configuration option.
//
// You can read more here:
// https://on.cypress.io/configuration
// ***********************************************************

// Import commands.js using ES2015 syntax:
import './commands'

// Alternatively you can use CommonJS syntax:
// require('./commands')

Cypress.on('uncaught:exception', (err, runnable) => {
    // jquery throws this error quite frequently on backend_uri hits
    if (err.message.indexOf('r.shift is not a function') > -1) {
        return false
    }
    return err
})

// Preserve the october admin cookie. Prefer manual logout url for this one.
Cypress.Cookies.defaults({
    preserve: ['admin_auth']
})
