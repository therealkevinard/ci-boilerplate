/// <reference types="cypress" />

declare namespace Cypress {
    interface Chainable {
        /**
         * Login to October Backend via UI form
        */
        loginUi(username: string, password: string): Chainable<Element>

        /**
         * October Logout action
         */
        logout(): Chainable<Element>

        /**
         * Frontend User login
         */
        frontendLoginUi(username: string, password: string): Chainable<Element>

        /**
         * Use login ui if cookie isn't preset, otherwise reuse admin session
         */
        loginIfNeeded():Chainable<Element>

        /**
         * Given fieldVals - a k/v object of name:value - will find inputs with $name, clear, and key-in $value
         * Ideally, should run inside cy.within($form)
         */
        transposeFormData(fieldValues:any):Chainable<Element>

        /**
         * Wrapper around cy.get to make data-key="value" more legible
         * @param key
         * @param value
         */
        getByData(key:string, value: string): Chainable<Element>
    }
}
