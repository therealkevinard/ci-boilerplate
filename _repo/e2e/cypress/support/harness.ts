/**
 * test users
 */
export const testUsers = {
    admin: {username: 'admin', password: 'admin'},
    customer: {username: 'test+001@domain.tld', password: 'test+001@domain.tld'}
}

/**
 * Naive route presence test generator:
 * - visits admin route
 * - asserts page title includes ${title}
 * @param route
 * @param title
 */
export const generateRoutesTests = (route: string, title: string) => {
    it(`${route} entity: has routes for list and create`, () => {
        cy.visit(route)
        cy.title().should('include', title)
        cy.visit(`${route}/create`)
        cy.title().should('include', `New ${title}`)
    })
}
