// ***********************************************
// This example commands.js shows you how to
// create various custom commands and overwrite
// existing commands.
//
// For more comprehensive examples of custom
// commands please read more here:
// https://on.cypress.io/custom-commands
// ***********************************************
//
//
// -- This is a parent command --
// Cypress.Commands.add("login", (email, password) => { ... })
//
//
// -- This is a child command --
// Cypress.Commands.add("drag", { prevSubject: 'element'}, (subject, options) => { ... })
//
//
// -- This is a dual command --
// Cypress.Commands.add("dismiss", { prevSubject: 'optional'}, (subject, options) => { ... })
//
//
// -- This will overwrite an existing command --
// Cypress.Commands.overwrite("visit", (originalFn, url, options) => { ... })

import '@testing-library/cypress/add-commands';

Cypress.Commands.add('loginByPost', (username, password) => {
    Cypress.log({
        name: 'loginByForm',
        message: `${username} | ${password}`,
    })

    return cy.request({
        method: 'POST',
        url: '/login',
        form: true,
        body: {
            username,
            password,
        },
    })
})

Cypress.Commands.add('loginUi', (username, password) => {
    cy.visit('/rwm_admin')
    cy.get('input[name="login"]').type(username)
    cy.get('input[name="password"]').type(`${password}{enter}`)
    return
})

Cypress.Commands.add('logout', () => {
    cy.visit('/rwm_admin/backend/auth/signout')
    return
})

Cypress.Commands.add('loginIfNeeded', () => {
    cy.getCookie('admin_auth').then(auth => {
        if (!auth) {
            cy.loginUi('admin', 'admin')
        }
    })
})

Cypress.Commands.add('frontendLoginUi', (username, password) => {
    // login link works
    cy.getByData('t', 'link-login').click({force: true})

    // login form works
    cy.get('[data-t="form-login"]')
        .within(() => {
            cy.get('input[name="login"]').type(`${username}`)
            cy.get('input[name="password"]').type(`${password}{enter}`)
        })
    cy.wait(1000)
    return
})

Cypress.Commands.add('transposeFormData', (fieldValues) => {
    for (let field in fieldValues) {
        cy.get(`input[name="${field}"]`)
            .clear()
            .type(fieldValues[field])
    }
    return
})

Cypress.Commands.add('getByData', (key, value) => {
    return cy.get(`[data-${key}="${value}"]`)
})
