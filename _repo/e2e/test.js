/**
 * Wraps standard cy:run with marge handling.
 */

const rmrf = require('rimraf')
const cypress = require('cypress')
const marge = require('mochawesome-report-generator')
const { merge } = require('mochawesome-merge')

const generateReport = (options) => {
    return merge(options).then(report => marge.create(report, options))
}

const reportOptions = {
    files: ['./mochawesome-report/*.json']
}

rmrf('./mochawesome-report', (err) => {
    cypress.run({
        browser: 'chrome'
    })
        .then(
            () => {
                generateReport()
            },
            error => {
                generateReport()
                console.error(error)
                process.exit(1)
            }
        )
})
